node-opencv (7.0.0+git20200316.f0a03a4b-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 22 Apr 2022 14:09:25 -0000

node-opencv (7.0.0+git20200316.f0a03a4b-1) unstable; urgency=low

  * New upstream release.
  * Add debian/patches/0002_port_to_axios_from_request.patch:
    - Drop debian/patches/drop-request-test.patch
    - Add node-axios to Build-Depends.
    - Port the test to axios so we can test it rather than drop the test.
  * Add debian/patches/0003_port_to_nodejs14.patch:
    - Porting to nodejs 14 (Closes: #1007843)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Fri, 08 Apr 2022 02:10:07 +0800

node-opencv (7.0.0+git20200310.6c13234-4) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Contact from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.1, no changes needed.

  [ Yadd ]
  * Add missing build dependency on dh addon.
  * Update standards version to 4.6.0, no changes needed.
  * Add "Rules-Requires-Root: no"
  * Add debian/gbp.conf
  * Modernize debian/watch
    * Fix filenamemangle
    * Fix GitHub tags regex
  * Drop dependency to nodejs
  * Drop test dependency to deprecated node-request (Closes: #958680)

 -- Yadd <yadd@debian.org>  Fri, 31 Dec 2021 12:45:07 +0100

node-opencv (7.0.0+git20200310.6c13234-3) unstable; urgency=medium

  * Team upload
  * MA: same
  * enable salsa CI

 -- Bastien Roucariès <rouca@debian.org>  Sat, 18 Sep 2021 13:30:24 +0000

node-opencv (7.0.0+git20200310.6c13234-2) unstable; urgency=medium

  * Fix OpenCV::ReadImageAsync segfault (Closes: #987364).
    Thanks to Jochen Sprickerhof.

 -- Jérémy Lal <kapouer@melix.org>  Fri, 30 Apr 2021 14:18:17 +0200

node-opencv (7.0.0+git20200310.6c13234-1) unstable; urgency=medium

  * Team upload
  * New upstream version 7.0.0+git20200310.6c13234 (Closes: #962825)
  * Drop 0003_port_opencv4.patch, applied upstream

 -- Jérémy Lal <kapouer@melix.org>  Mon, 15 Jun 2020 14:58:13 +0200

node-opencv (6.0.0+git20190322.db093cb2-7) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * Fix autopkgtest fail on arm64 (Closes: #951108)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Fri, 14 Feb 2020 09:47:03 +0800

node-opencv (6.0.0+git20190322.db093cb2-6) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * autopkgtest: add restrictions allow-stderr to avoid dpkg-architecture
    prints warning messages to stderr

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 11 Feb 2020 15:48:18 +0800

node-opencv (6.0.0+git20190322.db093cb2-5) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * Fix autopkgtest fail - dpkg-architecture stderr

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 11 Feb 2020 01:23:17 +0800

node-opencv (6.0.0+git20190322.db093cb2-4) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * Fix autopkgtest fail.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 10 Feb 2020 00:34:36 +0800

node-opencv (6.0.0+git20190322.db093cb2-3) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * Don't install object files.
  * Fix uses-deprecated-adttmp.
  * Fix nodejs-module-installed-in-usr-lib

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 09 Feb 2020 01:59:59 +0800

node-opencv (6.0.0+git20190322.db093cb2-2) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * Fix autopkgtest dependencies.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 09 Feb 2020 00:58:21 +0800

node-opencv (6.0.0+git20190322.db093cb2-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * New upstream version 6.0.0+git20190322.db093cb2
  * Remove unnecessary patches
  * Porting to OpenCV4 (Closes: #922587)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 28 Oct 2019 03:59:13 +0800

node-opencv (6.0.0+git20180416.cfc96ba0-3) unstable; urgency=medium

  * Team upload

  [ Xavier Guimard ]
  * Add dh_installexamples -Xtmp/ to make build reproductible. Thanks to
    Chris Lamb (Closes: #924462)

  [ Utkarsh Gupta ]
  * Add patch to fix CVE-2019-10061 (Closes: #925571)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 27 Mar 2019 04:27:41 +0530

node-opencv (6.0.0+git20180416.cfc96ba0-2) unstable; urgency=medium

  * Team upload
  * Remove unneeded dependency versions
  * Rebuilt against libnode-dev
  * Add upstream/metadata
  * Update description
  * Set hardening flags
  * Fix autopkgtest failures on an unbuild tree and test installed files
  * Install examples in the right place

 -- Xavier Guimard <yadd@debian.org>  Fri, 25 Jan 2019 14:45:46 +0100

node-opencv (6.0.0+git20180416.cfc96ba0-1) unstable; urgency=low

  * Use upstream git repo.
    - Support node 10 (Closes: #919192)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 17 Jan 2019 04:33:36 +0800

node-opencv (6.0.0-2) unstable; urgency=low

  * Fixing debian/control package descriptions.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 12 Dec 2018 00:32:46 +0800

node-opencv (6.0.0-1) unstable; urgency=low

  * Initial release (Closes: #870349)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 10 Dec 2018 19:30:10 +0800

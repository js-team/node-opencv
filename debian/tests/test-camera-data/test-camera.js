var cv = require('opencv');

try {
    var camera = new cv.VideoCapture('debian/tests/test-camera-data/data1.jpg');
    var multiarchpath = process.env["DEB_BUILD_MULTIARCH"];
    var xmlpath = "/usr/lib/" + multiarchpath + "/nodejs/opencv/data/haarcascade_frontalface_default.xml";
    camera.read(function(err, im) {
	if (err) throw err;
	console.log(im.size())
	if (im.size()[0] > 0 && im.size()[1] > 0){
	    console.log("image is ok");
	    console.log("use "+xmlpath);
	    im.detectObject(xmlpath, {}, function(err2, faces){
		if (err2) {
		    throw err2;
		}
		if (faces.length <= 0) {
		    console.error("no face");
		}
		for (var i = 0; i < faces.length; i++) {
		    var face = faces[i];
		    console.log("face at (" + face.x + ", " + face.y + ") - (" + (face.x+face.width) + ", " + (face.y+face.height) + ")");
		}
	    });
	} else {
	    console.error("image size is 0");
	}
    });
} catch (e){
  console.error("Couldn't start camera:", e)
}
